<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package theme_name_change_me
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <header class="header">

    <div class="wrapper">

      <nav class="nav">
        <div class="nav__logo">
          <a class="link-img" href="/"><img src="<?php echo IMAGES; ?>logo.svg" alt="Logo"></a>
        </div>
        <ul class="nav__list">
          <li class="nav__item">
            <a class="nav__link" href="/">
              Home page
            </a>
          </li>
          <li class="nav__item">
            <a class="nav__link" href="/sub/">
              Sub page
            </a>
          </li>
        </ul>
      </nav>

    </div>

  </header>
