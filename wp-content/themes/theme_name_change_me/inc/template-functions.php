<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package theme_name_change_me
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function theme_name_change_me_body_classes( $classes ) {
  // Adds a class of hfeed to non-singular pages.
  if ( ! is_singular() ) {
    $classes[] = 'hfeed';
  }

  // Adds a class of no-sidebar when there is no sidebar present.
  if ( ! is_active_sidebar( 'sidebar-1' ) ) {
    $classes[] = 'no-sidebar';
  }

  return $classes;
}
add_filter( 'body_class', 'theme_name_change_me_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */

function theme_name_change_me_pingback_header() {
  if ( is_singular() && pings_open() ) {
    echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
  }
}
add_action( 'wp_head', 'theme_name_change_me_pingback_header' );

/**
 * Get post date
 */

function theme_name_change_me_post_date($post_id = false, $date_format = 'Y.m.d') {
  if (!$post_id) {
    $post_id = get_the_ID();
  }
  $date = get_the_date( $date_format, $post_id );

  return $date;
}

/**
 * Truncate
 */

function theme_name_change_me_get_utf8_truncate( $string, $max_chars = 200, $append = "…" ) {
  $string = wp_filter_nohtml_kses($string);
  $string = html_entity_decode($string);
  $short_title = mb_substr($string,0,$max_chars,'UTF-8');

  if (strlen( utf8_decode( $string ) ) > $max_chars){
    $short_title .= $append;
    return $short_title;
  } else {
    return $short_title;
  }
}
