<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package theme_name_change_me
 */

?>

  <footer class="footer">

    <div class="wrapper">

      <div class="copyright">
        <p class="copyright__text">
          COPYRIGHT &copy; 2018 YS AND PARTNERS ALL RIGHTS RESERVED
        </p>
      </div>

    </div>

  </footer>

<?php wp_footer(); ?>

</body>
</html>
