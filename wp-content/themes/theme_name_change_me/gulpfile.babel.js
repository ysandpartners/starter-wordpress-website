// --------------------------------------------
// Dependencies
// --------------------------------------------

import gulp from 'gulp';

import stylus from 'gulp-stylus';
import postcss from 'gulp-postcss';
import autoprefixer from 'gulp-autoprefixer';
import cssnano from 'cssnano';
import pxtorem from 'postcss-pxtorem';
import sourcemaps from 'gulp-sourcemaps';

import babel from 'gulp-babel';
import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import rename from 'gulp-rename';

import imagemin from 'gulp-imagemin';

import browserSync from 'browser-sync';
import changed from 'gulp-changed';
import plumber from 'gulp-plumber';
import play from 'play';

// Paths
const src = './',
  dest = './';

const styleSrcMain = `${src}assets/styles/main.styl`,
  styleSrc = `${src}assets/styles/**/*.styl`,
  styleDest = `${dest}`,
  scriptSrc = `${src}assets/js/source/*.js`,
  vendorSrc = `${src}assets/js/source/vendors/*.js`,
  scriptDest = `${dest}assets/js/build/`,
  imgSrc = `${src}assets/img/source/**/*`,
  imgDest = `${dest}assets/img/build/`,
  styleWatch = `${dest}style.css`,
  scriptWatch = `${dest}assets/js/*.js`;

// Error handler
const onError = function(err) {
  //play.sound('/Users/ysap_sh/Code/audio/theoffice/problem.wav');
  console.log(err.toString());
  this.emit('end');
};

// --------------------------------------------
// Stand Alone Tasks
// --------------------------------------------

// Process styles
gulp.task('styles', () => {
  const processors = [autoprefixer, pxtorem, cssnano];
  return gulp
    .src(styleSrcMain)
    .pipe(
      plumber({
        errorHandler: onError
      })
    )
    .pipe(sourcemaps.init())
    .pipe(stylus())
    .pipe(postcss(processors))
    .pipe(
      rename({
        basename: 'style'
      })
    )
    .pipe(sourcemaps.write('.'))

    .pipe(gulp.dest(styleDest));
});

// Process JS
gulp.task('scripts', () => {
  return gulp
    .src(scriptSrc)
    .pipe(
      plumber({
        errorHandler: onError
      })
    )
    .pipe(sourcemaps.init())
    .pipe(
      babel({
        presets: ['@babel/preset-env']
      })
    )
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(scriptDest));
});

// Process vendor JS
gulp.task('vendors', () => {
  return gulp
    .src(vendorSrc)
    .pipe(plumber())
    .pipe(concat('vendors.js'))
    .pipe(uglify())
    .pipe(gulp.dest(scriptDest));
});

// Process images
gulp.task('images', () => {
  return gulp
    .src(imgSrc)
    .pipe(imagemin())
    .pipe(gulp.dest(imgDest));
});

// Watch for changes
gulp.task('watch', function() {
  // Serve files from the root of this project
  browserSync.init({
    // server: {
    //   baseDir: dest
    // },
    proxy: 'ysap2020.local',
    notify: false
  });

  gulp.watch(styleSrc, gulp.series('styles'));
  gulp.watch(scriptSrc, gulp.series('scripts'));
  gulp.watch(vendorSrc, gulp.series('vendors'));
  gulp.watch(imgSrc, gulp.series('images'));
  gulp.watch([styleWatch, scriptWatch]).on('change', browserSync.reload);
});

// use default task to launch Browsersync and watch JS files
gulp.task('default', gulp.series('styles', 'scripts', 'vendors', 'images', 'watch'));
