<?php
/**
 * Template Name: Home
 *
 * The template for the website's home page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme_name_change_me
 */

get_header();
?>

  <main class="main wrapper">

    <?php
    while ( have_posts() ) :
      the_post();

      get_template_part( '/template-parts/content', 'home' );

    endwhile; // End of the loop.
    ?>

  </main><!-- #main -->

<?php
get_footer();
